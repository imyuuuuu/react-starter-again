import "./App.css";
import { Button, Badge } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import MyNav from "./components/MyNav";
import MyCard from "./components/MyCard";
import Product from "./pages/Product";
import Sample from "./pages/Sample";
import Counter from "./pages/Counter";

function App() {
  return (
    <>
      <MyNav />
      {/* <Product/> */}
      {/* <Sample/> */}
      <Counter/>
      <h2>Show Git</h2>
    </>
  );
}

export default App;
