import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import MyCard from "../components/MyCard";
import { Col, Row, Container } from "react-bootstrap";

const Product = () => {
  return (
    <>
      <Container>
        <Row>
          <Col sm={12} md={6} lg={4}>
            <MyCard />
          </Col>
          <Col sm={12} md={6} lg={4}>
            <MyCard />
          </Col>
          <Col sm={12} md={6} lg={4}>
            <MyCard />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Product;
