import { isDisabled } from "@testing-library/user-event/dist/utils";
import React, { Component } from "react";

export class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
    };
  }

  render() {
    return (
      <div className="container">
        <h1 className="text-center">{this.state.counter}</h1>
        <div className="text-center">
          <button
            className="btn btn-success me-2"
            onClick={() => {
              this.setState({
                counter: this.state.counter + 1,
              });
            }}
          >
            Increase
          </button>
          <button
            className="btn btn-danger me-2"
            onClick={() => {
              this.setState({
                counter: (this.state.counter > 0)? this.state.counter - 1 : 0
              });
            }}
          >
            Decrease
          </button>
          <button
            className="btn btn-dark"
            onClick={() => {
              this.setState({
                counter: (this.state.counter = 0),
              });
            }}
          >
            Reset
          </button>
        </div>
      </div>
    );
  }
}

export default Counter;
