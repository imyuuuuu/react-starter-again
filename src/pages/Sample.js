import React, { Component } from "react";
// Statefull Component ( Class Component )

export class Sample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "Tra",
      gender: "Male",
    };
  }

  render() {
    return (
      <div className="container">
        <h1>Person Information</h1>
        <h3>Name : {this.state.name} </h3>
        <h3>Gender : {this.state.gender} </h3>
        <button
          className="btn btn-warning"
          onClick={() =>
            this.setState({
              name: 'Leakhena',
              gender: "Female",
            })
          }
        >
          Change Infor
        </button>
      </div>
    );
  }
}

export default Sample;
